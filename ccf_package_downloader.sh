#!/bin/bash

##
## ccf_package_downloader.sh
##
## Copyright (c) 2023, Washington University School of Medicine 
##
## Released under the Simplified BSD.
##

## SPECIFY ANY DESIRED DEFAULTS HERE ##
HOST="" # (e.g. db.humanconnectome.org)
OUTDIR="."
SCRIPTDIR="`dirname $(readlink -f $0)`"
#echo "SCRIPTDIR=$SCRIPTDIR"
PROJECTS="" # (e.g. HCP_1200)
SESSIONS=""
RESOURCES=""
FILES=""

while true; do 
    case "$1" in
      --help | -h | -\?)
	printf "\nCCF XNAT Package Downloader:  Downloads session-level resource packages from an XNAT\n"
	printf "\n$(basename $0) [options] [LIST-SESSIONS|LIST-PACKAGES|LIST-FILES|LIST-FILEPATHS-ONLY|DOWNLOAD-FILES]\n\n"
	printf "   Options\n\n"
	printf "      -h, --help             Show help information and exit\n"
	printf "      -H, --host             <host>\n"
	printf "      -u, --user             <user>\n"
	printf "      -p, --pw               <password>\n"
	printf "      -P, --project          <project>\n"
	printf "      -S, --sessions         <regex matching experiment labels (MR Sessions Only) OR path to file containing regexes>\n"
	printf "      -R, --packages         <regex matching package labels>\n"
	printf "      -o, --output-dir       <directory for downloads>\n"
	printf "      -q, --quiet            Don't show download progress bar (DOWNLOAD-FILES only)\n"
	printf "      -qq, --really-quiet    Don't show download progress bar or information about files being downloaded/skipped (DOWNLOAD-PACKAGES only)\n"
	printf "      --skip-existing-files  Don't re-download files that already exist\n"
	printf "\n   Notes regarding regular expressions\n"
	printf "\n       There is effectively an implicit ^.* at the begining of any specified regular expression and a .*$ at the end unless an"
	printf "\n       an explicit ^ or $ is specified.  So, the regular expressions become more of a \"contains\" sort of expression.  So, "
	printf "\n       for example, when sessions are of the form [SubjId]_[3T|7T], 7T sessions can be specified with '--sessions \"7T\".'\n"
	printf "\n   Example Usage\n\n"
	printf "       ccf_package_downloader.sh --host intradb.humanconnectome.org --project CCF_HCA_STG --sessions \"HCA(8888888|9999999)_V1_MR\" --packages \"PreprocStrucRecommended\" -o \"/path/to/output/directory\" --skip-existing-files DOWNLOAD-FILES\n\n"
	printf "       ccf_package_downloader.sh --host intradb.humanconnectome.org --project CCF_HCA_STG --sessions \"HCA(8888888|9999999)_V1_MR\" --packages \"(Diffusion|Structural)Recommended\" -o \"/path/to/output/directory\" --skip-existing-files DOWNLOAD-FILES\n\n"
	printf "       ccf_package_downloader.sh --host intradb.humanconnectome.org --project CCF_HCA_STG --sessions \"HCA(8888888|9999999)_V1_MR\" --packages \"(Diffusion|Structural)Recommended\" -o \"/path/to/output/directory\" LIST-FILEPATHS-ONLY\n\n"
	printf "       ccf_package_downloader.sh --host intradb.humanconnectome.org --project CCF_HCA_STG LIST-PACKAGES\n\n"
	exit 0
	;;
      --host | -H)
        HOST=$2
	shift
	shift
        ;;
      --user | -u)
        USR=$2
	shift
	shift
        ;;
      --pw | -p)
        PW=$2
	shift
	shift
        ;;
      --project | -P)
        PROJECTS=$2
	shift 
	shift 
        ;;
      --sessions | -S)
        SESSIONS=$2
	shift 
	shift 
        ;;
      --packages | -R)
        PACKAGES=$2
	shift 
	shift 
        ;;
      --files | -F)
        FILES=$2
	shift 
	shift 
        ;;
      --output-dir | -o)
        OUTDIR=$2
	shift 
	shift 
        ;;
      --quiet | -q)
        QUIET="TRUE"
	shift 
        ;;
      --really-quiet | -qq)
        REALLY_QUIET="TRUE"
        QUIET="TRUE"
	shift 
        ;;
      --skip-existing-files)
        SKIP_EXISTING="TRUE"
	shift 
        ;;
      -*)
	echo "Invalid parameter ($1)"
	exit 1
        ;;
      *)
	break 
        ;;
    esac
done

if ! command -v  jq &> /dev/null; then
	echo -e "\nERROR:  This program requires the jq json parser to be available (https://stedolan.github.io/jq/download/)\n"
	exit 1
fi

COMMAND=$1

if [ "$HOST" == "" ] ; then
	echo "ERROR:  You must specify a host"
	exit
fi
if [ "$PROJECTS" == "" ] ; then
	echo "ERROR:  You must specify a project or comma-separated list of projects"
	exit
fi

if ! [[ $COMMAND =~ ^(LIST-SESSIONS|LIST-PACKAGES|LIST-FILES|LIST-FILEPATHS-ONLY|DOWNLOAD-FILES)$ ]] ; then
	echo "ERROR:  Invalid command.  Valid commands are LIST-SESSIONS, LIST-PACKAGES, LIST-FILES, LIST-FILEPATHS-ONLY, DOWNLOAD-FILES"
	exit
fi

RENEW_JSESSION_ID () {
	HTTP_CODE=`curl -s -w "%{http_code}\n" --cookie JSESSIONID=$JSESSIONID https://${HOST}/data/projects/$PROJ -o /dev/null`
	if [ "$HTTP_CODE" != "200" ] ; then
        	echo "JSESSIONID is invalid or expired - exiting (HTTP_CODE=$HTTP_CODE)"
		read -p "Enter Username:  " USR;
		read -s -p "Enter Password:  " PW;
		echo ""
	else
		return
	fi
	JSESSIONID=`curl -s -v -u $USR:$PW https://${HOST}/data/JSESSIONID 2> /dev/null`
	echo "JSESSIONID successfully set.  Run the following line in your session to set an environment variable for future runs."
	echo "export JSESSIONID=$JSESSIONID"
	HTTP_CODE=`curl -s -w "%{http_code}\n" --cookie JSESSIONID=$JSESSIONID https://${HOST}/data/projects/$PROJ -o /dev/null`
	if [ "$HTTP_CODE" != "200" ] ; then
        	echo "JSESSIONID is invalid or expired - exiting (HTTP_CODE=$HTTP_CODE)"
       		exit
	fi
}

#echo "HOST=$HOST"
#echo "JSESSIONID=$JSESSIONID"
#echo "PROJ=$PROJ"
#echo "USR=$USR"
#echo "SESSIONS=$SESSIONS"
#echo "FILES=$FILES"
#echo "COMMAND=$COMMAND"
#echo "OUTDIR=$OUTDIR"
#exit

#check_filename () {
#	REGEX_FILE=$1
#	FILE_URI=$2
#	for REGEX in `cat $REGEX_FILE`; do
#		if [[ "$FILE_URI" =~ $REGEX ]] ; then
#			echo "TRUE"
#			return
#		fi
#	done
#	echo "FALSE"
#}

if ! [[ $COMMAND =~ ^(LIST-SESSIONS|LIST-PACKAGES)$ ]] ; then
	if [ "$PACKAGES" == "" ] ; then
		echo "ERROR:  You must specify a value for packages"
		exit
	fi
fi

if [[ "$PROJECTS" =~ AABC_HCA_JOINED ]] ; then
	PROJECTS="AABC_STG,CCF_HCA_STG"
fi
OLDIFS=$IFS
IFS=',' read -r -a PROJ_ARR <<< "$PROJECTS"

DO_PROJECT() {

	RENEW_JSESSION_ID

	SESSION_LIST=`curl -s --cookie JSESSIONID=$JSESSIONID https://${HOST}/data/projects/${PROJ}/experiments?xsiType=xnat:imageSessionData\&format=csv\&columns=label,subject_label,URI | awk -F, '{print $3,$1,$2,$4,$5,$6}' OFS=',' | tail -n +2 | sort`

	PACKAGE_LIST=`curl -s --cookie JSESSIONID=$JSESSIONID https://${HOST}/xapi/ccfNdaTransfer/${PROJ}/packages | jq -r '.[]' | sort`
	PACKAGE_LIST=`echo "$PACKAGE_LIST" | grep -E "$PACKAGES"`
	if [ $COMMAND == "LIST-PACKAGES" ] ; then
		echo "$PACKAGE_LIST"
		return
	fi
	
	URL_PACKAGES=`echo "$PACKAGE_LIST" | grep -E "$PACKAGES" | tr '\n' ',' | sed -e "s/, *$//"`
	
	for SESSION_INFO in `echo "$SESSION_LIST"`; do
		SESSION_LBL=`echo "$SESSION_INFO" | cut -d',' -f1`
		SUBJECT_LBL=`echo "$SESSION_INFO" | cut -d',' -f3`
		if [ ${#SESSIONS} -gt 0 ] ; then
			if [[ -f "${SESSIONS}" ]] ; then
				if [[ $(check_filename ${SESSIONS} ${SESSION_LBL}) != "TRUE" ]] ; then
					continue
				fi
			elif ! [[ "${SESSION_LBL}" =~ ${SESSIONS} ]] ; then
				continue;
			fi
		fi
		if [ $COMMAND == "LIST-SESSIONS" ] ; then
			echo "$SESSION_INFO"
			continue
		fi
		#echo "curl -s --cookie JSESSIONID=$JSESSIONID https://${HOST}/xapi/ccfNdaTransfer/${PROJ}/packages/$URL_PACKAGES/sessions/$SESSION_LBL/files"
		FILE_LIST="`curl -s --cookie JSESSIONID=$JSESSIONID https://${HOST}/xapi/ccfNdaTransfer/${PROJ}/packages/$URL_PACKAGES/sessions/$SESSION_LBL/files`";
		for FILE_INFO in `echo "$FILE_LIST" | jq -c '.[]'`; do
			FILE_PATH="`echo "$FILE_INFO" | jq -r '.filePath'`"
			FILE_URI="`echo "$FILE_INFO" | jq -r '.fileURI'`"
			if [ $COMMAND == "LIST-FILES" ] ; then
				echo "$FILE_INFO"
				continue
			elif [ $COMMAND == "LIST-FILEPATHS-ONLY" ] ; then
				FPVAR=`echo "$FILE_URI" | sed -e "s/^.*\/files\///"`
				if ! [[ $FPVAR =~ "^$SESSION_LBL.*$" ]] ; then
					FPVAR="$SESSION_LBL/$FPVAR"
				fi
				echo "$FPVAR"
				continue
			elif [ $COMMAND == "DOWNLOAD-FILES" ] ; then
				FPVAR=`echo "$FILE_URI" | sed -e "s/^.*\/files\///"`
				if ! [[ $FPVAR =~ "^$SESSION_LBL.*$" ]] ; then
					FPVAR="$SESSION_LBL/$FPVAR"
				fi
				FILE_PATH="$FPVAR"
				FILE_DIR=`dirname "$FILE_PATH"`
				if [ "$SKIP_EXISTING" == "TRUE" ] && [ -f $OUTDIR/$FILE_PATH ] ; then
					if ! [ "$REALLY_QUIET" == "TRUE" ] ; then
						echo "Skipping:  $OUTDIR/$FILE_PATH (File exists and --skip-existing-files is specified)"
					fi
					continue
				fi
				if ! [[ $FILE_PATH =~ ^\/*($SUBJECT_LBL|$SESSION_LBL)\/.*$ ]] ; then
					FILE_PATH="$SESSION_LBL/$FILE_PATH"
					FILE_DIR="$SESSION_LBL/$FILE_DIR"
				fi
				if [ ! -d $OUTDIR/$FILE_DIR ] ; then
					mkdir -p $OUTDIR/$FILE_DIR
				fi
				if ! [ "$REALLY_QUIET" == "TRUE" ] ; then
					echo "Downloading file:  $FILE_PATH"
				fi
				if [ "$QUIET" == "TRUE" ] ; then
					curl -s --cookie JSESSIONID=$JSESSIONID https://${HOST}{$FILE_URI} -o $OUTDIR/$FILE_PATH
				else
					curl --cookie JSESSIONID=$JSESSIONID --progress-bar https://${HOST}{$FILE_URI} -o $OUTDIR/$FILE_PATH
				fi
			fi
		done
	done
}

if ! [[ $COMMAND =~ "LIST-PACKAGES" ]] ; then
	for PROJ in "${PROJ_ARR[@]}"; do
  		DO_PROJECT 
	done
else
	PKGVAR=""
	for PROJ in "${PROJ_ARR[@]}"; do
  		PKGVAR="$PKGVAR\n`DO_PROJECT`" 
	done
	echo -e "$PKGVAR" | sort -u | egrep -v "^ *$"
fi

