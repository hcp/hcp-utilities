#!/bin/bash

host=hcpi-shadow16.nrg.wustl.edu
freeze_proj=AABC_FZ1
output=AABC_FZ1
jsessionid="$1"

process_session() {
	#echo $1
	sess=`echo $1 | cut -d, -f1`
	proj=`echo $1 | cut -d, -f5`
	echo $sess $proj

	packages=`./aabc_package_downloader.sh -H $host -j $jsessionid -P $proj -S $sess LIST-PACKAGES`

	for pack in `echo $packages`; do
		#echo $pack
		
		group=""
		if [[ $pack == *"Unproc"* ]]; then
			group="Unproc"
		elif [[ $pack == *"Proc"* ]]; then
			group="Recommended"
		elif [[ $pack == *"NotRecommended"* ]]; then
			group="NotRecommended"
			echo "Skipping ${sess} ${pack}..."
			continue
		elif [[ $pack == *"Recommended"* ]]; then
			group="Recommended"
		elif [[ $pack == *"Extended"* ]]; then
			group="Extended"
			
			if [[ $pack != "StructuralExtended" ]]; then
				echo "Skipping ${sess} ${pack}..."
				continue
			fi
		fi

		echo "Packaging ${sess} ${pack}..."

		packout=${output}/${sess}/${group}
		zip=${packout}/${sess}_${pack}.zip

		mkdir -p $packout
	
		if ! test -f ${zip}.md5; then
			files=`./aabc_package_downloader.sh -H $host -j $jsessionid -P $proj -S $sess --packages $pack LIST-FILES | cut -d: -f2 | cut -d\" -f2`

			cinabdir=/data/intradb/archive/CinaB/${proj}/
			cinabfiles=""
	
			#echo $files

			for fil in `echo $files`; do
				cinabfiles="$cinabfiles ${cinabdir}$fil"
			#	echo $fil
			done
			
			zip $zip ${cinabfiles}
			md5sum $zip > ${zip}.md5
			sed -i "s@${packout}/@@g" ${packout}/${sess}_${pack}.zip.md5 # Non-standard @ separator used for sed to get around / in paths
		else
			echo "Found existing ${sess} ${pack} md5, continuing..."
		fi
	done
}

sessions=`./aabc_package_downloader.sh -H $host -j $jsessionid -P $freeze_proj LIST-SESSIONS`
sessions=`echo $sessions`

MAXJOBS=3
for session in `echo $sessions`; do
	#process_session $session # Uncomment and comment the rest of this loop for multi-threaded
	
	( process_session $session ) &

	while (( $(jobs -r | wc -l) >= MAXJOBS )); do 
		sleep 10 
	done 
done
